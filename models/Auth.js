const querys = require('../models/db/dbQuerys/querys')
const tableName = 'users_db';

class Auth {
    static async checkPhoneNumberForUnique(phone) {
        console.log('phone', phone)
        return await querys.findByMultyNamePromise(tableName, {phone: phone}, ['id'])
    }
}

module.exports = Auth;
