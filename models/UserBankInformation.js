const querys = require('../models/db/dbQuerys/querys')
const tableName = 'user_bank_information';

class UserBankInformation {

    static async findOrCreate(shaba, cart_number, user_id) {
        try {
            const existingRows = await querys.findByMultyNamePromise(
                tableName,
                { shaba, cart_number, user_id },
                ['id']
            )

            if( existingRows.length !== 0 ) {
                return existingRows[0].id
            }

            const creatingRow = await querys.insert2vPromise(tableName, { shaba, cart_number, user_id })

            if(creatingRow) {
                return creatingRow.insertId
            }

            return null
        } catch (e) {
            return e
        }
    }
}

module.exports = UserBankInformation;
