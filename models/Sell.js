const tableName = 'sale_v2';
const querys = require('../models/db/dbQuerys/querys')
const statics = require("../static");
const request = require("request");

class Sell {
    static async createOrder(user_id, user_bank_information_id) {
        try {
            return await querys.insert2vPromise(tableName, {
                user_id, user_bank_information_id
            })
        } catch (err) {
            throw new Error(err)
        }
    }

    static makeTransaction(userId, orderId) {
        return new Promise((resolve, reject) => {
            const options = {
                url: 'https://secure2.imoneysecure.com/vapp/make-transaction/',
                method: "POST",
                headers: {
                    'Accept': 'application/json'
                },
                form: {
                    apiKey: statics.flipSellApiKey,
                    callbackUrl: statics.MAIN_URL+'/sell/save-transaction',
                    redirect_url : statics.MAIN_URL+'/profile/sale',
                    details: {
                        userId,
                        orderId
                    }
                }
            };
          // console.log('options',options)
            request(options, function (error, response, body) {
              // console.log('error', error)
              // console.log('response', response)
              // console.log('body', body)
                const result = JSON.parse(body);
                if (!error && response.statusCode === 200) {
                    if(result.status === 0) {
                        querys.updatePromise(tableName,
                            {
                                where: { id: orderId },
                                values: { transactionId: result.data.transactionId }
                            })
                            .then(()=> {
                                resolve(result.data.gatewayUrl)
                            })
                            .catch((e)=> {
                              // console.log('e',e)
                            })

                    }
                }
            });
        })
    }

    static getOrderByTransactionId(transactionId) {
        return new Promise((resolve, reject) => {
            querys.findByMultyNamePromise(
                tableName,
                {transactionId},
                ['id']
                )
                .then((data)=> {
                  // console.log('.........data',data)
                    resolve(data)
                })
                .catch((e) => {
                  // console.log('e',e)
                    reject(e)
                })
        });
    }

    static verifyTransaction(transactionId) {
        return new Promise((resolve, reject) => {
            const options = {
                url: 'https://secure2.imoneysecure.com/vapp/do-verify/',
                method: "POST",
                headers: {
                    'Accept': 'application/json'
                },
                form: {
                    apiKey: statics.flipSellApiKey,
                    authority: transactionId
                }
            };

            request(options, function (error, response, body) {
                const result = JSON.parse(body);
                if (!error && response.statusCode === 200) {
                  // console.log(result)
                  // console.log(body)
                    // body.amount
                    querys.updatePromise(tableName,{
                        values: { amount: result.data.amount, status_api: 1 },
                        where: { transactionId }
                    })
                        .then((data)=> {
                          // console.log('.........data',data)
                            resolve(data)
                        })
                        .catch(()=> {
                          // console.log('e',e)
                            reject(e)
                        })
                }
            });
        })
    }
}

module.exports = Sell;
