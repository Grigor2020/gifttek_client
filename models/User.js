const tableName = 'users_db';
const querys = require('../models/db/dbQuerys/querys')

class Sell {
    static async getUserByToken(token) {
        let data;

        try {
            data = await querys.findByMultyNamePromise(tableName, {
                token,
            }, ['id','f_name'])
        } catch (err) {
            throw new Error(err)
        }

        return data
    }
}

module.exports = Sell;
