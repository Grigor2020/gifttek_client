var express = require('express');
var router = express.Router();
const request = require('request');
var statics = require('../../static');
var multer = require('multer');

var upload = multer({
    fileSize:1024 * 1024*1024,
    fileFilter: (req, file, cb) => {
        if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg") {
            cb(null, true);
        } else {
            cb(null, false);
            return cb(new Error('Only .png, .jpg and .jpeg format allowed!'));
        }
    }//size of u file
});
const fs = require('fs');

const imagePath = [__dirname, '/../../public/images/passports/'].join('')
console.log('imagePath', imagePath)

upload.storage = multer.diskStorage({
    destination: imagePath,
    // destination: './public/images/passports/',
    filename: function (req, file, cb) {
        let type = file.mimetype.split("/");
        let random = Math.round(Math.random()*1299999);
        let fullName = random + '-' + Date.now()+"."+type[1];
        cb(null, fullName);
    }
});


router.get('/orders', function(req, res, next) {
    var options = {
        url: statics.API_URL+'/api/v1/orders/all',
        headers: {
            'Accept': 'application/json',
            'authorization' : statics.API_AUTH
        }
    };
    if(typeof req.siteData.userInfo !== "undefined"){
        options.headers.void = req.siteData.userInfo[0].token
    }else{
        options.headers.guest = req.siteData.guestInfo
    }
    request.post(
        options, function (err, httpResponse, body) {
            var jsonBody = JSON.parse(body);
            res.render(`profile/orders`, {
                title: 'Express',
                siteData : req.siteData,
                data:jsonBody.data
            });
        }
    )

});

router.get('/c2c', function(req, res, next) {
    var options = {
        url: statics.API_URL+'/api/v1/c2c/all',
        headers: {
            'Accept': 'application/json',
            'authorization' : statics.API_AUTH
        }
    };
    if(typeof req.siteData.userInfo !== "undefined"){
        options.headers.void = req.siteData.userInfo[0].token
    }else{
        res.redirect('/')
    }
    request.post(
        options, function (err, httpResponse, body) {
            var jsonBody = JSON.parse(body);
            if(!jsonBody.error){
              // console.log('jsonBody',jsonBody)
                res.render(`profile/c2c`, {
                    title: 'Express',
                    siteData : req.siteData,
                    data:jsonBody.data
                });
            }
        }
    )

});

router.get('/sale', function(req, res, next) {
    var options = {
        url: statics.API_URL+'/api/v1/sale/all',
        headers: {
            'Accept': 'application/json',
            'authorization' : statics.API_AUTH
        }
    };
    if(typeof req.siteData.userInfo !== "undefined"){
        options.headers.void = req.siteData.userInfo[0].token
    }else{
        options.headers.guest = req.siteData.guestInfo
    }
    request.post(
        options, function (err, httpResponse, body) {
            var jsonBody = JSON.parse(body);
            // if(!jsonBody.error){
          // console.log('jsonBody',jsonBody)
            res.render(`profile/sale`, {
                title: 'Express',
                siteData : req.siteData,
                data:jsonBody.data
            });
            // }
        }
    )

});

router.get('/orders/success', function(req, res, next) {
    res.render(`profile/success`, {
        title: 'Express',
        siteData : req.siteData
    });
});

router.get('/orders/failed', function(req, res, next) {
    res.render(`profile/failed`, {
        title: 'Express',
        siteData : req.siteData
    });
});

router.get('/user', function(req, res, next) {
  // console.log('req.siteData.userInfo[0]',req.siteData.userInfo[0])
    const options = {
        url: statics.API_URL+'/api/v1/profile/user/passport/all',
        headers: {
            'Accept': 'application/json',
            'authorization': statics.API_AUTH
        }
    };
    if(typeof req.siteData.userInfo !== "undefined"){
        options.headers.void = req.siteData.userInfo[0].token
    }else{
        options.headers.guest = req.siteData.guestInfo
    }
    request(options, function (error, response, body) {
        const info = JSON.parse(body);
        if(!info.error){
          // console.log('info',info)
            res.render(`profile/user`, {
                title: 'Express',
                siteData : req.siteData,
                passports : info.data
            });
        }
    });

});

var cpUpload = upload.fields([
    { name: 'user_img', maxCount: 1 }]);

router.post('/user/add-info',cpUpload,function (req,res,next){
    console.log('cpUpload',cpUpload)
    console.log('req.body',req.body)
    console.log('req.files',req.files)
    const options = {
        url: ''+statics.API_URL+'/api/v1/profile/user/addpassport',
        method : "POST",
        headers: {
            'Accept': 'application/json',
            'authorization': statics.API_AUTH
        },
        form: {
            bankCardNumber : req.body.bank_card_number,
            image : req.files.user_img[0].filename
        }
    };
    if(typeof req.siteData.userInfo !== "undefined"){
        options.headers.void = req.siteData.userInfo[0].token
    }else{
        options.headers.guest = req.siteData.guestInfo
    }
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            const result = JSON.parse(body);
          // console.log('result',result)
            // if(!result.error){
                res.redirect('/profile/user');
            // }else {
            //     res.json({error:true,msg:msg})
            // }
        }
    });
})

module.exports = router;
