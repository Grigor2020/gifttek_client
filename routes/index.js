var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    // console.log('siteData',req.siteData.categories)
  res.render('index',
      {
          title: 'Express',
          siteData : req.siteData
      });
});
// router.get('/contact', function(req, res, next) {
//   res.render('contact', { title: 'Express' });
// });
// router.get('/faq', function(req, res, next) {
//   res.render('faq', { title: 'Express' });
// });
// router.get('/card-detail', function(req, res, next) {
//   res.render('cardDetail', {
//       title: 'Express',
//       categories : categories.categories,
//       siteData : req.siteData
//   });
// });
// router.get('/registration', function(req, res, next) {
//   res.render('registration', { title: 'Express' });
// });
// router.get('/complaint', function(req, res, next) {
//   res.render('complaint', { title: 'Express',  siteData : req.siteData });
// });
// router.get('/basket', function(req, res, next) {
//   res.render('basket', { title: 'Express' });
// });

module.exports = router;
