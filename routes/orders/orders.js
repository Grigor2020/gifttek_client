const express = require('express');
const router = express.Router();
const request = require('request');
const statics = require('../../static');
const ZarinpalCheckout = require('zarinpal-checkout');
const zarinpal = ZarinpalCheckout.create('914235fe-49b3-4dee-b0fe-3c2d6523dac3', false);

router.get('/create', function(req, res) {
    if(typeof req.siteData.userInfo === "undefined"){
        res.redirect("/reg")
    }else{
        request.post(
            {
                url: statics.API_URL+'/api/v1/orders/create',
                headers: {
                    'Accept': 'application/json',
                    'void': req.siteData.userInfo[0].token,
                    'authorization' : statics.API_AUTH
                }
            }
            , function (err, httpResponse, body) {
                const jsonBody = JSON.parse(body);
                if(!jsonBody.error){
                    const orderId = jsonBody.data.insertId;
                    const amount = jsonBody.data.total;
                    var back = statics.MAIN_URL+'/orders/done/'+orderId+'/'+amount+'';
                    zarinpal.PaymentRequest({
                        Amount: amount,
                        CallbackURL: back,
                        Description: 'GIFT card',
                        Email: '',
                        Mobile: req.siteData.userInfo[0].phone
                    }).then(function (response) {
                        if (response.status == 100) {
                            res.redirect(response.url);
                        }
                    }).catch(function (err) {
                      // console.log(err);
                    });
                }
            }
        )
    }
});


router.get('/done/:orderId/:amount',function (req,res,next){
  // console.log('..........req.params......',req.params)
  // console.log('.......req.query.........',req.query)
  // console.log('req.siteData',req.siteData.userInfo)
    // var results = {
    //     status:1,
    //     ref_id : response.RefID,
    //     cart : response.ExtraDetail.Transaction.CardPanMask,
    //     order_id:req.params.orderId
    // }
    zarinpal.PaymentVerification({
        Amount: req.params.amount, // In Tomans
        Authority: req.query.Authority,
    }).then(response => {
      // console.log('.............response',response)
        if (response.status === -21 || response.status === -51) {

            // res.json({Verified:"Empty"});
            // res.end();
            var results = {
                status:0,
                ref_id : null,
                // cart : response.ExtraDetail.Transaction.CardPanMask,
                order_id:req.params.orderId
            }
            // var results = {
            //     status:1,
            //     ref_id : 124567888,
            //     cart : 4578890000146579,
            //     order_id:req.params.orderId
            // }
            var options =
                {
                    url: statics.API_URL+'/api/v1/orders/success',
                    headers: {
                        'Accept': 'application/json',
                        'authorization' : statics.API_AUTH
                    },
                    form : results
                }
            request.post(
                options
                , function (err, httpResponse, body) {
                    var jsonBody = JSON.parse(body);
                  // console.log('.........jsonBody',jsonBody)
                    res.redirect("/profile/orders/failed");
                }
            )
        } else {
          // console.log('response',response)
            var results = {
                status:1,
                ref_id : response.RefID,
                // cart : response.ExtraDetail.Transaction.CardPanMask,
                order_id:req.params.orderId
            }
            // var results = {
            //     status:1,
            //     ref_id : 124567888,
            //     cart : 4578890000146579,
            //     order_id:req.params.orderId
            // }
            var options =
                {
                    url: statics.API_URL+'/api/v1/orders/success',
                    headers: {
                        'Accept': 'application/json',
                        'authorization' : statics.API_AUTH
                    },
                    form : results
                }
            request.post(
                options
                , function (err, httpResponse, body) {
                    var jsonBody = JSON.parse(body);
                  // console.log('.........jsonBody',jsonBody)
                    if(!jsonBody.error){
                        res.redirect("/profile/orders/success");
                    }else{
                        res.redirect("/profile/orders/failed");
                    }
                }
            )
        }
    }).catch(err => {
        console.error(err);
    });
})
module.exports = router;
