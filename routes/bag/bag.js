var express = require('express');
var router = express.Router();
const request = require('request');
var statics = require('../../static');

router.get('/', function(req, res, next) {
  // console.log('req.siteData.userInfo',req.siteData.userInfo)
  // console.log('req.siteData.guestInfo',req.siteData.guestInfo)
    var options =
        {
            url: statics.API_URL+'/api/v1/cart/all',
            headers: {
                'Accept': 'application/json',
                'authorization' : statics.API_AUTH
            }
        }
    if(typeof req.siteData.userInfo !== "undefined"){
        options.headers.void = req.siteData.userInfo[0].token
    }else{
        options.headers.guest = req.siteData.guestInfo
    }
  // console.log('options',options)
    request.post(
        options
        , function (err, httpResponse, body) {
            var jsonBody = JSON.parse(body);
          // console.log('jsonBody',jsonBody.data)
            if(!jsonBody.error){
              // console.log('1',jsonBody.data)
              // console.log('2',jsonBody.data.cart)
                res.render(`bag/bag`, {
                    title: 'Express',
                    siteData : req.siteData,
                    items:jsonBody.data.cart
                });
            }
        }
    )

});


router.post('/add', function(req, res, next) {
    var options =
        {
            url: statics.API_URL+'/api/v1/cart/add',
            headers: {
                'Accept': 'application/json',
                'authorization' : statics.API_AUTH
            },
            form: {
                prodId : req.body.prodId,
                count:req.body.counts
            }
        }
        if(typeof req.siteData.userInfo !== "undefined"){
            options.headers.void = req.siteData.userInfo[0].token
        }else{
            options.headers.guest = req.siteData.guestInfo
        }
      // console.log('options',options)
    request.post(
        options
        , function (err, httpResponse, body) {
            var jsonBody = JSON.parse(body);
            if(!jsonBody.error){
                res.json(jsonBody)
            }
        }
    )
});


router.post('/update-count', function(req, res, next) {
    var options =
        {
            url: statics.API_URL+'/api/v1/cart/count',
            headers: {
                'Accept': 'application/json',
                'authorization' : statics.API_AUTH
            },
            form: {
                prodId : req.body.prodId,
                actionType : req.body.type
            }
        }
        if(typeof req.siteData.userInfo !== "undefined"){
            options.headers.void = req.siteData.userInfo[0].token
        }else{
            options.headers.guest = req.siteData.guestInfo
        }
    request.post(
        options
        , function (err, httpResponse, body) {
            var jsonBody = JSON.parse(body);
            if(!jsonBody.error){
                res.json(jsonBody)
            }
        }
    )
});

router.post('/remove', function(req, res, next) {
    var options =
        {
            url: statics.API_URL+'/api/v1/cart/remove',
            headers: {
                'Accept': 'application/json',
                'authorization' : statics.API_AUTH
            },
            form: {
                prodId : req.body.prodId
            }
        }
        if(typeof req.siteData.userInfo !== "undefined"){
            options.headers.void = req.siteData.userInfo[0].token
        }else{
            options.headers.guest = req.siteData.guestInfo
        }
    request.post(
        options
        , function (err, httpResponse, body) {
            var jsonBody = JSON.parse(body);
            if(!jsonBody.error){
                res.json(jsonBody)
            }
        }
    )
});


module.exports = router;
