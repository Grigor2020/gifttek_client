var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
    res.render(`complaint/complaint`, {
        siteData : req.siteData
    });
});


module.exports = router;
