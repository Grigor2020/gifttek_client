var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
    res.render(`about/about`, {
        siteData : req.siteData
    });
});


module.exports = router;
