var express = require('express');
var router = express.Router();
const request = require('request');
var statics = require('../../static');

router.get('/:id', function(req, res, next) {
    if(req.params.id == '27' || req.params.id == '29') {
        res.redirect('/')
        return false;
        // res.render('goToArzanGift', {
        //     siteData: req.siteData
        // });
        // return
    }
    var allCategories = req.siteData.categories;
    var thisCategorie;
    for(var i = 0; i < allCategories.length; i++){
        if(allCategories[i].id == req.params.id){
            thisCategorie = allCategories[i]
        }
    }
    // if(thisCategorie !== '29') {
    let options = {
        url: statics.API_URL + '/api/v1/catalog',
        headers: {
            'Accept': 'application/json',
            'authorization': statics.API_AUTH
        },
        form: {
            cat_id: req.params.id
        }
    }
    if(typeof req.siteData.userInfo !== "undefined"){
        options.headers.void = req.siteData.userInfo[0].token
    }else{
        options.headers.guest = req.siteData.guestInfo
    }
  // console.log('options',options)
        request.post(
            options, function (err, httpResponse, body) {
                var jsonBody = JSON.parse(body);
              // console.log('jsonBody.data', jsonBody)
                if (req.params.id == '27') {
                    res.render(`card/categorieMoney`, {
                        title: 'Express',
                        siteData: req.siteData,
                        cards: typeof jsonBody.data === "undefined" ? [] : jsonBody.data
                    });
                } else if(req.params.id == '29'){
                    // console.log(req.siteData.categories)
                    let renderPage = 'card/c2c_verify';
                    if(typeof req.siteData.userInfo !== "undefined"){
                        renderPage = 'card/categorieC2CNew';
                    }
                    res.render(renderPage, {
                        title: 'Express',
                        siteData: req.siteData,
                        flipRate: jsonBody.flipRate,
                        cards : typeof jsonBody.cards === "undefined" ? [] : jsonBody.cards,
                        numbers: typeof jsonBody.data === "undefined" ? [] : jsonBody.data[0]
                    });
                }else{
                    res.render(`card/categorieStatic`, {
                        title: 'Express',
                        siteData: req.siteData,
                        cards: typeof jsonBody.data === "undefined" ? [] : jsonBody.data
                    });
                }
            }
        )
    // }else{
    //     request.post(
    //         {
    //             url: statics.API_URL + '/api/v1/catalog',
    //             headers: {
    //                 'Accept': 'application/json',
    //                 'authorization': statics.API_AUTH
    //             },
    //             form: {
    //                 cat_id: req.params.id
    //             }
    //         }, function (err, httpResponse, body) {
    //             var jsonBody = JSON.parse(body);
    //             if (req.params.id == '27') {
    //               // console.log('jsonBody.data', jsonBody.data)
    //                 res.render(`card/categorieMoney`, {
    //                     title: 'Express',
    //                     siteData: req.siteData,
    //                     cards: typeof jsonBody.data === "undefined" ? [] : jsonBody.data
    //                 });
    //             } else {
    //                 res.render(`card/categorieStatic`, {
    //                     title: 'Express',
    //                     siteData: req.siteData,
    //                     cards: typeof jsonBody.data === "undefined" ? [] : jsonBody.data
    //                 });
    //             }
    //         }
    //     )
    // }
});

router.get('/:id/:amount', function(req, res, next) {
    var allCategories = req.siteData.categories;
    var thisCategorie;
    for(var i = 0; i < allCategories.length; i++){
        if(allCategories[i].id == req.params.id){
            thisCategorie = allCategories[i]
        }
    }
    thisCategorie.amount = req.params.amount;
    res.render(`card/cardStaticThis`, {
        title: 'Express',
        siteData : req.siteData,
        categorie : thisCategorie
    });
});
module.exports = router;
