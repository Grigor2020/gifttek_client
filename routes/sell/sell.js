const express = require('express');
const router = express.Router();
const request = require('request');
const statics = require('../../static');
const sellModel = require('../../models/Sell')
const userModel = require('../../models/User')
const UserBankInformation = require('../../models/UserBankInformation')

router.get('/', function(req, res, next) {

    // res.json({url: "https://google.com"})

  // console.log('siteData',req.siteData)
    res.render('sell/sell',
        {
            siteData : req.siteData
        });
});

router.get('/external', function(req, res, next) {

    // res.json({url: "https://google.com"})

  // console.log('siteData',req.siteData)
    res.render('sell/external',
        {
            siteData : req.siteData
        });
});

router.get('/take-action', function(req, res, next) {
    if(typeof req.siteData.userInfo !== "undefined") {
        res.render('sell/sellVerify',
            {
                siteData : req.siteData
            });
    } else {
        res.redirect(statics.MAIN_URL)
    }

});

router.post('/set-carts', async function(req, res, next) {
    if(typeof req.siteData.userInfo !== "undefined") {
        const user = await userModel.getUserByToken(req.siteData.userInfo[0].token);

        const {shaba, cardNumber, sellCategorie} = req.body;

        if(sellCategorie !== '27') {
            return res.json({error:true,msg:'779236655'});
        }

        const userBankInformationId = await UserBankInformation.findOrCreate(shaba, cardNumber, user[0].id)
      // console.log('userBankInformationId',userBankInformationId)

        const createdOrder = await sellModel.createOrder(user[0].id, userBankInformationId);

        const makeTransaction = await sellModel.makeTransaction(user[0].id, createdOrder.insertId);

      // console.log('createdOrder',createdOrder)
        return res.json({error: false, makeTransaction, orderId: createdOrder.insertId, mainUrl: statics.MAIN_URL})
    } else {
        res.json({error:true,msg:'779236654'});
    }
});

router.post('/save-transaction', async function(req, res, next) {
    if(req.body.transactionId) {
        const transactionId = req.body.transactionId;
        const order = await  sellModel.getOrderByTransactionId(transactionId);
      // console.log('order',order)
        const orderId = order[0].id

        const verifyTransaction = await sellModel.verifyTransaction(transactionId);

        if(verifyTransaction) {
            res.redirect(statics.MAIN_URL+'/profile/sale')
        }

    }
});

function setResponseObject(error, errorMsg, data = null) {
    return {
        error,
        errorMsg,
        data
    }
}
//*********************************--- OLD CODE ---****************************************

router.post('/set-code', function(req, res, next) {
    if(typeof req.siteData.userInfo !== "undefined") {
        const options = {
            url: '' + statics.API_URL + '/api/v1/sale/add-code',
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'void' : req.siteData.userInfo[0].token,
                'authorization': statics.API_AUTH
            },
            form: {
                categorie: req.body.categorie,
                alfa: req.body.alfa,
                beta: req.body.beta
            }
        };
        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                const result = JSON.parse(body);
              // console.log('result', result)
                if(!result.error){
                     res.json({error:false})
                }else{
                    if(result.msg == '8456268453165156'){
                        res.json({error:true, msg : 'invalid token'});
                    }
                }
            }
        });
    }else{
        res.json({error:true,msg:'779236653'});
    }
});

module.exports = router;
