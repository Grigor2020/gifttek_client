var express = require('express');
var router = express.Router();
const request = require('request');
var statics = require('../../static');

router.post('/adds', function(req, res, next) {
  // console.log('okkkkkk')
    var options =
        {
            url: statics.API_URL+'/api/v1/c2c/add',
            headers: {
                'Accept': 'application/json',
                'authorization' : statics.API_AUTH
            },
            form: {
                cat_id : req.body.cat_id,
                bank_card_number : req.body.bank_card_number,
                c2cUserBankCardNumber : req.body.c2cUserBankCardNumber,
                amount : req.body.amount
            }
        }
    if(typeof req.siteData.userInfo !== "undefined"){
        options.headers.void = req.siteData.userInfo[0].token
    }else{
        options.headers.guest = req.siteData.guestInfo
    }
  // console.log('options',options)
    request.post(options, function (err, httpResponse, body) {
        var jsonBody = JSON.parse(body);
      // console.log('C2C jsonBody',jsonBody)
        if (!jsonBody.error) {
            res.json({error:false})
        }else{
            var msg = '456791';
            if(jsonBody.msg == '84651258961655336'){
                msg = '458999'
            }
            res.json({error:true,msg:msg})
        }
    })
});

router.post('/phone_verify', function(req, res, next) {
    var phone = req.body.c2cVerPhone;
    var options =
        {
            url: statics.API_URL+'/api/v1/c2c/phone-verification',
            headers: {
                'Accept': 'application/json',
                'authorization' : statics.API_AUTH
            },
            form: {
                phone : phone
            }
        }
    if(typeof req.siteData.userInfo !== "undefined"){
        options.headers.void = req.siteData.userInfo[0].token
    }else{
        options.headers.guest = req.siteData.guestInfo
    }
    request.post(options, function (err, httpResponse, body) {
        var jsonBody = JSON.parse(body);
      // console.log('.........jsonBody',jsonBody)
        if (!jsonBody.error) {
            if(jsonBody.msg == 'ok' || jsonBody.msg == '30001'){
                res.cookie('void', jsonBody.data['token'],{expires: new Date(Date.now() + 900000000),path: '/'});
                let url = '/';
                if(jsonBody.data.status == 0){
                    url = '/reg/phone-verification';
                    res.cookie('vin', jsonBody.data['token'],{expires: new Date(Date.now() + 900000000),path: '/'});
                }
                res.json({error:false,errCode:'111',redirectUrl : url})
                res.end();
            }else if(jsonBody.msg == '199'){
                res.json({error:false,errCode:'222',phone : phone})
                res.end();
            }
        }else{
            res.json({error:true,msg:jsonBody.msg})
        }
    })
});


module.exports = router;
