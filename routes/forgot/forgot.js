var express = require('express');
var router = express.Router();
const request = require('request');
var statics = require('../../static');


router.get('/', function(req, res, next) {
    res.render(`forgot/typeEmail`, {
        title: 'Express',
        siteData : req.siteData,
        errorNumber : false
    });
});
router.get('/insert-code', function(req, res, next) {
    res.render(`forgot/typeCode`, {
        title: 'Express',
        siteData : req.siteData
    });
});

router.get('/new-password', function(req, res, next) {
    res.render(`forgot/typeNewPassword`, {
        title: 'Express',
        siteData : req.siteData
    });
});



router.post('/send-number', function(req, res, next) {
    if(req.body.phone.length > 0 && req.body.phone !== ""){
        var options =
            {
                url: statics.API_URL+'/api/v1/forgot/phone',
                headers: {
                    'Accept': 'application/json',
                    'authorization' : statics.API_AUTH
                },
                form: {
                    phone : req.body.phone
                }
            }
        request.post(
            options
            , function (err, httpResponse, body) {
                var jsonBody = JSON.parse(body);
                if(!jsonBody.error){
                    res.redirect('/forgot-password/insert-code')
                }else{
                    res.render(`forgot/typeEmail`, {
                        title: 'Express',
                        siteData : req.siteData,
                        errorNumber : true
                    });
                }
            }
        )
    }
});


router.post('/send-code', function(req, res, next) {
    if(req.body.code > 0){
        var options =
            {
                url: statics.API_URL+'/api/v1/forgot/phone/code',
                headers: {
                    'Accept': 'application/json',
                    'authorization' : statics.API_AUTH
                },
                form: {
                    code : req.body.code
                }
            }
        request.post(
            options
            , function (err, httpResponse, body) {
                var jsonBody = JSON.parse(body);
                if(!jsonBody.error){
                    res.cookie('fin', jsonBody.data,{expires: new Date(Date.now() + 900000000),path: '/'});
                    res.json(jsonBody)
                }
            }
        )
    }
});


router.post('/new-passwords', function(req, res, next) {
    if(req.body.password === req.body.repassword){
        var toks = req.cookies['fin'];
        var options =
            {
                url: statics.API_URL+'/api/v1/forgot/password/new',
                headers: {
                    'Accept': 'application/json',
                    'authorization' : statics.API_AUTH
                },
                form: {
                    pass : req.body.password,
                    repass : req.body.repassword,
                    toks:toks
                }
            }
          // console.log('options',options)
        request.post(
            options
            , function (err, httpResponse, body) {
                var jsonBody = JSON.parse(body);
                if(!jsonBody.error){
                    res.clearCookie('void');
                    res.redirect('/');
                    res.end();
                }
            }
        )
    }
});



module.exports = router;
