var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
    res.render(`contact/contact`, {
        siteData : req.siteData
    });
});


module.exports = router;
