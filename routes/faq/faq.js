var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
    res.render(`faq/faq`, {
        siteData : req.siteData
    });
});


module.exports = router;
