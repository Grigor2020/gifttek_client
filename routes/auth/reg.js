var express = require('express');
var router = express.Router();
const request = require('request');
var static = require('../../static');

router.get('/already', function(req, res, next) {
  // console.log('req.siteData',req.siteData)
    res.render('already_reg',
        {
            title: 'Express' ,
            siteData : req.siteData
        });
});

router.get('/', function(req, res, next) {
  // console.log('req.siteData',req.siteData)
    res.render('registration',
        {
            title: 'Express' ,
            siteData : req.siteData
        });
});

router.get('/phone-verification', function(req, res, next) {
    if(typeof req.cookies['vin'] !== "undefined" && req.cookies['vin'] !== ""){
        res.render('phone_verification',
            {
                title: 'Express' ,
                siteData : req.siteData
            });
    }else{
        res.redirect('/')
    }
});
router.post('/send-code', function(req, res, next) {
    if(typeof req.cookies['vin'] !== "undefined" && req.cookies['vin'] !== ""){
        const formData = {
            "code" :req.body.code
        };
      // console.log('formData',formData)
        request.post(
            {
                url: static.API_URL+'/api/v1/sign-up/code',
                headers: {
                    'Accept': 'application/json',
                    'authorization' : static.API_AUTH
                },
                form: formData
            }, function (err, httpResponse, body) {
                var jsonBody = JSON.parse(body);
              // console.log('jsonBody',jsonBody)
                if(!jsonBody.error){
                    var token = jsonBody.data[0]['token'];
                    res.clearCookie('vin');
                    res.cookie('void', token,{expires: new Date(Date.now() + 900000000),path: '/'});
                    res.json(jsonBody);
                }else{
                    res.json(jsonBody);
                }
            }
        )
    }else{
        res.redirect('/')
    }
});

router.post('/get-code', function(req, res, next) {
    if(typeof req.cookies['vin'] !== "undefined" && req.cookies['vin'] !== "" && typeof req.cookies['void'] !== "undefined" && req.cookies['void'] !== ""){
      // console.log('req.siteData.userInfo[0]',req.siteData.userInfo[0])
        if(req.siteData.userInfo[0].status == '0'){
            const formData = {
                "phone" :req.siteData.userInfo[0].phone
            };
            request.post(
                {
                    url: static.API_URL+'/api/v1/sign-up/resend',
                    headers: {
                        'Accept': 'application/json',
                        'authorization' : static.API_AUTH
                    },
                    form: formData
                }, function (err, httpResponse, body) {
                    var jsonBody = JSON.parse(body);
                    if(!jsonBody.error){
                        res.json({error:false});
                    }else{
                        res.json({error:true});
                    }
                }
            )
        }
    }else{
        res.redirect('/')
    }
});

module.exports = router;
