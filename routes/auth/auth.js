var express = require('express');
var router = express.Router();
const request = require('request');
var static = require('../../static');
var Cookies = require('cookies')
const authModel = require('../../models/Auth')
/* GET users listing. */
router.post('/sign-up', function(req, res, next) {
    if ( req.body.firstName.length > 0 &&
        req.body.lastName.length > 0 &&
        req.body.phone.length > 0 &&
        req.body.password.length > 0
    ){
        const formData = {
            "f_name" :req.body.firstName,
            "l_name" :  req.body.lastName,
            "phone" :  req.body.phone,
            "password" :  req.body.password
        };
        console.log('formData', formData)
        request.post(
            {
                url: static.API_URL+'/api/v1/sign-up',
                headers: {
                    'Accept': 'application/json',
                    'authorization' : static.API_AUTH
                },
                form: formData
            }, function (err, httpResponse, body) {
                console.log('auth jsonBody', jsonBody)
                var jsonBody = JSON.parse(body);

                if(!jsonBody.error){
                    var token = jsonBody.data.token;
                    res.cookie('vin', token,{expires: new Date(Date.now() + 900000000),path: '/'});
                    // res.cookie('void', token,{expires: new Date(Date.now() + 900000000),path: '/'});
                    res.redirect('/reg/phone-verification');
                }else{
                    if(jsonBody.msg === '199') {
                        res.redirect('/reg/already');
                    }
                    res.json({error:true, errorCode : jsonBody.msg})
                    res.redirect('/');
                }
                res.end();
            }
        )
    }
});

router.post('/sign-in', function(req, res, next) {
    // console.log('asdasdd',req.body)
    if (
        req.body.phone.length > 0 &&
        req.body.password.length > 0
    )
    {
        const formData = {
            "phone" :req.body.phone,
            "password" :  req.body.password,
            "guest" :  req.cookies['gin']
        };
        request.post(
            {
                url: static.API_URL+'/api/v1/sign-in',
                headers: {
                    'Accept': 'application/json',
                    'authorization' : static.API_AUTH
                },
                form: formData
            }, function (err, httpResponse, body) {
                var jsonBody = JSON.parse(body);
              // console.log('.........jsonBody',jsonBody)
                if(!jsonBody.error){
                    res.cookie('void', jsonBody.data.user[0]['token'],{expires: new Date(Date.now() + 900000000),path: '/'});
                    let url = '/';
                    if(jsonBody.data.user[0].status == 0){
                        url = '/reg/phone-verification';
                        res.cookie('vin', jsonBody.data.user[0]['token'],{expires: new Date(Date.now() + 900000000),path: '/'});
                    }
                    res.json({error:false,url : url})
                    // res.redirect(url);
                    res.end();
                }else{
                    res.json({error:true})
                    res.end();
                }

            }
        )
    }
});

router.get('/logout', function(req, res, next) {
    Cookies.set('void', {expires: Date.now()});
    Cookies.set('vin', {expires: Date.now()});
    Cookies.set('fin', {expires: Date.now()});
  // console.log('Cookies',Cookies)
    // res.clearCookie('void');
    // res.clearCookie('vin');
    // res.clearCookie('fin');

    // res.redirect('/');
    // res.end();

});

router.post('/check-phone', async function(req, res, next) {
    try {
        const user = await authModel.checkPhoneNumberForUnique(req.body.phone)
        if(user.length > 0) {
            res.json({error: true, userFind: true})
        } else {
            res.json({error: false, userFind: false})
        }
    } catch (e) {
        res.json({error: true, msg: e})
    }
});

module.exports = router;
