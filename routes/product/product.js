var express = require('express');
var router = express.Router();
const request = require('request');
var statics = require('../../static');

router.get('/:id', function(req, res, next) {
    request.post(
        {
            url: statics.API_URL+'/api/v1/product/single',
            headers: {
                'Accept': 'application/json',
                'authorization' : statics.API_AUTH
            },
            form: {
                prodId : req.params.id
            }
        }, function (err, httpResponse, body) {
            var jsonBody = JSON.parse(body);
            // if(!jsonBody.error){
          // console.log('jsonBody',jsonBody)
            res.render(`product/product`, {
                title: 'Express',
                siteData : req.siteData,
                product : typeof jsonBody.data === "undefined" ? [] : jsonBody.data
            });
            // }
        }
    )
});


module.exports = router;
