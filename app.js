var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var authRouter = require('./routes/auth/auth');
var regRouter = require('./routes/auth/reg');
var categoriesRouter = require('./routes/categories/categories');
var sellRouter = require('./routes/sell/sell');
var productRouter = require('./routes/product/product');
var bagRouter = require('./routes/bag/bag');
var profileRouter = require('./routes/profile/profile');
var contactRouter = require('./routes/contact/contact');
var faqRouter = require('./routes/faq/faq');
var aboutRouter = require('./routes/about/about');
var complaintRouter = require('./routes/complaint/complaint');
var forgotPasswordRouter = require('./routes/forgot/forgot');
var ordersRouter = require('./routes/orders/orders');
var termsRouter = require('./routes/terms/terms');
var c2cRouter = require('./routes/c2c/c2c');
var mytestRouter = require('./routes/mytest/mytest');
var mids = require('./middleware');
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.enable('trust proxy')
app.use(function (req, res, next) {
  req.siteData = {};
  // var ip = req.connection.remoteAddress;
  // console.log('ip',ip)
  res.setHeader("Access-Control-Allow-Headers", "x-access-token,void,authorization");;
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Credentials", true);
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT'); // OPTIONS, PATCH,
  res.setHeader('Cache-Control', 'no-cache'); // OPTIONS, PATCH,
  res.setHeader('X-Frame-Options', 'deny'); // OPTIONS, PATCH,
  res.removeHeader('Server');
  res.removeHeader('X-Powered-By');
  next()
})



app.use(function (req, res, next) {mids.checkUser(req, res, next)});
app.use(function (req, res, next) {mids.setGuest(req, res, next)});
app.use(function (req, res, next) {mids.checkGuest(req, res, next)});
app.use(function (req, res, next) {mids.getAllCategories(req, res, next)});

app.use(function (req,res,next){
  let vinCook = req.cookies['vin'];
  let voidCook = req.cookies['void'];
  if(req.originalUrl == '/auth/logout'){
    res.clearCookie('void');
    res.clearCookie('vin');
    res.clearCookie('fin');
    res.redirect('/');
  }

  if(typeof vinCook !== "undefined" && typeof voidCook !== "undefined"){
    if(req.originalUrl == '/reg/phone-verification' || req.originalUrl == '/reg/get-code' || req.originalUrl == '/reg/send-code'){
      next()
    }else {
      res.redirect('/reg/phone-verification');
    }
  }else{
    next()
  }
  // console.log('vinCook',vinCook)

})


app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/auth', authRouter);
app.use('/reg', regRouter);
app.use('/categories', categoriesRouter);
app.use('/sell', sellRouter);
app.use('/product', productRouter);
app.use('/bag', bagRouter);
app.use('/profile', profileRouter);
app.use('/contact', contactRouter);
app.use('/faq', faqRouter);
app.use('/about', aboutRouter);
app.use('/complaint', complaintRouter);
app.use('/forgot-password', forgotPasswordRouter);
app.use('/orders', ordersRouter);
app.use('/terms', termsRouter);
app.use('/c2c', c2cRouter);
app.use('/mytest', mytestRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
