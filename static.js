// const MAIN_URL = 'http://localhost:3010';
// const MAIN_URL = 'https://dev.arzancart.store';
const MAIN_URL = 'https://arzancart.store';
//
// const API_URL = 'http://localhost:3011';
const API_URL = 'https://api.arzancart.store';

// const dev_RUNNER_PORT = 3100;
const dev_RUNNER_PORT = 3010;

const API_AUTH = "6fe163ef7dd2de632f4776dbf6ca8eb11d9772f9";

const flipSellApiKey = '6d68a8c11fe04625';

module.exports.MAIN_URL = MAIN_URL;
module.exports.API_URL = API_URL;
module.exports.API_AUTH = API_AUTH;
module.exports.flipSellApiKey = flipSellApiKey;
module.exports.dev_RUNNER_PORT = dev_RUNNER_PORT;
