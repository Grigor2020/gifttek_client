const request = require('request');
var statics = require('./static');


const checkUser = function (req, res, next) {
    var cookie = req.cookies['void'];
    // console.log('......cookie',cookie)
    if(typeof cookie === 'undefined'){
        next();
    }else{
        request.post(
            {
                url: statics.API_URL+'/api/v1/check-user',
                headers: {
                    'Accept': 'application/json',
                    'authorization' : statics.API_AUTH,
                    'void' : cookie
                }
            }, function (err, httpResponse, body) {
                var jsonBody = JSON.parse(body);
                if(!jsonBody.error){
                    // console.log('1111111111111111',jsonBody.data.user)
                    req.siteData.userInfo = jsonBody.data.user;
                    if(jsonBody.data.user[0].status == '0'){
                        res.cookie('vin', jsonBody.data.user[0]['token'],{expires: new Date(Date.now() + 900000000),path: '/'});
                    }
                }else{
                    res.clearCookie('void');
                    res.clearCookie('vin');
                    res.clearCookie('fin');
                }
                next();
            }
        )
    }
}

const setGuest = function (req, res, next) {
    var cookie = req.cookies['gin'];
    if(typeof cookie === 'undefined'){
        var options = {
            url: statics.API_URL+'/api/v1/guest',
            headers: {
                'Accept': 'application/json',
                'authorization' : statics.API_AUTH
            }
        };
        // console.log('options',options)
        request.post(
            options
            , function (err, httpResponse, body) {
              // console.log('err',err)
                var jsonBody = JSON.parse(body);
                if(!jsonBody.error){
                    res.cookie('gin', jsonBody.data.token,{expires: new Date(Date.now() + 900000000),path: '/'});
                    req.siteData.guestInfo = jsonBody.data.token;
                }
                next();
            }
        )
    }else{
        req.siteData.guestInfo = cookie;
        next();
    }
}
const checkGuest = function (req, res, next) {
    var cookie = req.cookies['gin'];
    if(typeof cookie !== 'undefined'){
        var formData = {
            "guest" :cookie
        };
        request.post(
            {
                url: statics.API_URL+'/api/v1/guest/check',
                headers: {
                    'Accept': 'application/json',
                    'authorization' : statics.API_AUTH
                },
                form: formData
            }, function (err, httpResponse, body) {
                // var jsonBody = JSON.parse(body);
                // console.log('11.jsonBody',jsonBody)
                // if(!jsonBody.error){
                //     req.siteData.guestInfo = jsonBody.data;
                // }
                next();
            }
        )
    }else{
        next();
    }
}

const getAllCategories = function (req, res, next) {
    const options = {
        url: statics.API_URL+'/api/v1/categories',
        headers: {
            'Accept': 'application/json',
            'authorization': statics.API_AUTH
        }
    };
    request(options, function (error, response, body) {
        const info = JSON.parse(body);
        if(!info.error){
            req.siteData.categories = info.data;
        }
        next();
    });
}


module.exports.checkUser = checkUser;
module.exports.setGuest = setGuest;
module.exports.checkGuest = checkGuest;
module.exports.getAllCategories = getAllCategories;
