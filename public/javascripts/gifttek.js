$(function(){

    $('#sellCategorie').change(function () {
        $('#submitSell').css({display: 'flex'})
        $('#externalLinkGenerated').css({display: 'none'})
    })

    $('#submitSell').click(function () {
        let alfa = $('#alfaCode').val();
        let beta = $('#betaCode').val();
        let sellCategorie = $("#sellCategorie option:selected").attr("data-id")
        if(alfa.length === 16 && (beta.length === 22 || beta.length === 24)) {
            $('#saleCardError').css({display: "none"})
            let body = 'shaba='+alfa+'&cardNumber='+beta+'&sellCategorie='+sellCategorie;
            let url = '/sell/set-carts';
            requestPost( url, body, function () {
                if (this.readyState == 4) {
                    let result = JSON.parse(this.responseText);
                    let externalLinkGenerated = $('#externalLinkGenerated')
                    $('#submitSell').css({display: 'none'})
                    if(!result.error){
                        externalLinkGenerated.attr('href',result.makeTransaction)
                        externalLinkGenerated.attr('target','_blank')
                        const text = "برای ادامه اینجا کلیک کنید";
                        externalLinkGenerated.text(text)
                        externalLinkGenerated.css({display:'inline'})
                        $('#button-to-profile').css({display:'block'})
                        // window.location.href = result.mainUrl+'/sell/take-action';

                    }else{
                        if(result.msg === "779236655") {
                            externalLinkGenerated.attr('href','/sell/external')
                            const text = "برای ادامه اینجا کلیک کنید";
                            externalLinkGenerated.text(text)
                            externalLinkGenerated.css({display:'inline'})
                            $('#button-to-profile').css({display:'block'})
                        } else {
                            alert('Error!')
                        }
                    }
                }
            })
        } else {
            $('#saleCardError').css({display: "block"})
        }
    })

    $('#homeSlide').slick({
        responsive: [
            {
              breakpoint: 991,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 767,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
          ]
    });

    $(document).on('scroll', function(){
        if($(document).scrollTop() > 50){
            $("html").addClass("sticky");
        }
        else{
            $("html").removeClass("sticky");
        }
    });

    $('#userBtn').click(function(){
        $('header .user-info-popup').toggle(300);
    });

    $('.menu-btn').click(function(){
        const $this = $(this);
        if(!$this.hasClass('active')){
            $this.addClass('active');
            $('header nav ul').slideDown(300);
        }
        else{
            $this.removeClass('active');
            $('header nav ul').slideUp(300);
        }
    });

    $('.input-fields button').click(function(e){
        // e.preventDefault();
        const $this = $(this);
        // $('.input-field').each(function(){
        //     const $inputField = $(this);
        //     if($inputField.find('input').length > 0 || $inputField.find('textarea').length > 0){
        //         if(($inputField.find('input').length > 0 && $inputField.find('input').val().trim() != '') || ($inputField.find('textarea').length > 0 && $inputField.find('textarea').val().trim() != '')){
        //             $inputField.removeClass('mandatory');
        //             if($('.input-field.mandatory').length == 0){
        //               // console.log('in')
        //                 $this.closest('form').submit();
        //             }
        //         }
        //         else{
        //           // console.log('out')
        //             $inputField.not('.mandatory').addClass('mandatory');
        //         }
        //     }
        // })
        $('.cardOrderForm').submit();
    });
    $('#thisCardBag').click(function (){
        var prodId = $(this).attr('data-id');
        var counts = 1;
        requestPost('/bag/add','prodId='+prodId+'&counts='+counts+'',function (){
            if (this.readyState == 4) {
                let result = JSON.parse(this.responseText);
                if(!result.error){
                    window.location.href = '/bag';
                }
            }

        })
    })
    $('#thisFlipBag').click(function (){
        var prodId = $(this).prev('select').children("option:selected").attr('data-id');
        // console.log('prodId',prodId)
        if(typeof prodId !== "undefined" ) {
            var counts = 1;
            requestPost('/bag/add','prodId='+prodId+'&counts='+counts+'',function (){
                if (this.readyState == 4) {
                    let result = JSON.parse(this.responseText);
                    if(!result.error){
                        window.location.href = '/bag';
                    }
                }

            })
        }
    })
    $('.faq-section .item .tab-header a').click(function(){
        const $this = $(this);
        let tab = $this.closest('.item');
        let content = tab.find('.tab-content');
        if(!tab.hasClass('active')){
            $('.faq-section .item.active .tab-content').slideUp(300);
            $('.faq-section .item.active').removeClass('active');
            tab.addClass('active');
            content.slideDown(300);

        }
        else{
            content.slideUp(300);
            tab.removeClass('active');
        }
    });

    $('#loginBtn').click(function(){
        $('#loginPopUp').fadeIn(200);
        setTimeout(function(){
            $("#loginPopUp").addClass('active');
        },200);
    });

    $('#sloginButton').click(function () {
        var $errorField = $('#errorLogin');
        var phoneNUmber = $('#phoneLogin').val();
        var password = $('#passLogin').val();
        if(phoneNUmber.length > 8 && password.length > 6){
            $('.all_load').css({'display':'flex'});
            requestPost('/auth/sign-in','phone='+phoneNUmber+'&password='+password+'',function () {
                if (this.readyState == 4) {
                    let result = JSON.parse(this.responseText);
                  // console.log('result',result)
                    if(!result.error){
                        window.location.href = result.url;
                    }else{
                        $('.all_load').css({'display':'none'});
                        $errorField.css({'display':'block'})
                    }
                }
            })
        }else{
            $('.all_load').css({'display':'none'});
            $errorField.css({'display':'block'})
        }
    })


    $(document).on('click', function(e){
        if($('#loginPopUp.active').length > 0 && $(e.target).closest('.popup-outer').length === 0){
            $("#loginPopUp.active").removeClass('active');
            setTimeout(function(){
                $('#loginPopUp').fadeOut(200);
            },200);
        }
        if($(e.target).closest('#userBtn').length === 0 && $(e.target).closest('header .user-info-popup').length === 0){
            $('header .user-info-popup').fadeOut(300)
        }
    });

    $( ".label-image" ).click(function() {
        $(this).find(".img_file" )[0].click();
    });
    $( ".label-images" ).click(function() {
        $(this).find(".img_files" )[0].click();
    });
    function renderImage(file,x,setFileBlock){
        var reader = new FileReader();
        reader.onload = function(event){
            the_url = event.target.result;
          // console.log('the_url',the_url)
            setFileBlock.css('display','block');
            setFileBlock.attr('src',the_url);
        }
        reader.readAsDataURL(file);
    }

    $( ".img_file" ).change(function() {
        renderImage(this.files[0],this,$('.user_added_pic'))
    });
    $( ".img_files" ).change(function() {
        renderImage(this.files[0],this,$('.user_added_pics'))
    });

});

$(document).on('load', function(){
    if($(document).scrollTop() > 50){
        $("html").addClass("sticky");
    }
});


var isRequestedToCheckUser = false
$('#cardOrderForm').submit(async function(e){
    if(!isRequestedToCheckUser) {
        e.preventDefault();
        const firstNameField = $('#reg_firstname');
        const lastNameField = $('#reg_lastname')
        const phoneField = $('#reg_phone')
        const passwordField = $('#reg_password')
        const firstNameErrorField = $('#first_name_error');
        const lastNameErrorField = $('#last_name_error')
        const phoneErrorField = $('#phone_error')
        const passwordErrorField = $('#pass_error')

        firstNameErrorField.text('');
        lastNameErrorField.text('');
        phoneErrorField.text('');
        passwordErrorField.text('');

        const firstName = firstNameField.val()
        const lastName = lastNameField.val()
        const phone = phoneField.val().replace(/\s/g, '')
        const password = passwordField.val()
        if (
            validateFirstName(firstName) &&
            validateLastName(lastName) &&
            validatePhone(phone) &&
            validatePassword(password)
        ) {
            requestPost('/auth/check-phone', 'phone=' + phone, function () {
                if (this.readyState === 4) {
                    let result = JSON.parse(this.responseText);
                    if (result.userFind) {
                        $('#phone_error').text('با این شماره قبلا ثبت نام شده')
                        return false
                    } else {
                        isRequestedToCheckUser = true
                        $('#cardOrderForm').submit();

                    }
                }
            })
        }
    }
});

async function validatePhoneForUnique(phone) {
    return new Promise( resolve => {
        requestPost('/auth/check-phone', 'phone='+phone, function () {
            if (this.readyState === 4) {
                let result = JSON.parse(this.responseText);
                if(result.userFind) {
                    $('#phone_error').text('با این شماره قبلا ثبت نام شده')
                    resolve(false)
                } else {
                    resolve(true)
                }
            }
        })
    })
}

function validatePassword(password) {
    if(password.length < 6) {
        $('#pass_error').text('رمز عبور باید حداقل 6 کاراکتر باشد')
        return false
    }
    return true
}
function validatePhone(phone) {
    if(phone.length !== 11) {
        $('#phone_error').text('شماره تماس را به درستی وارد کنید')
        return false
    }
    return true
}

function validateFirstName(firstName) {
    if(firstName.length === 0) {
        $('#first_name_error').text('نام الزامی است')
        return false
    }
    return true
}

function validateLastName(lastName) {
    if(lastName.length === 0) {
        $('#last_name_error').text('نام خانوادگی الزامی است')
        return false
    }
    return true
}
