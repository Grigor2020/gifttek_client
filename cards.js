var statics = require('./static');
var categories = [
    {id: 26, name: "Flipmoney",name_pharsi: "فلیپ مانی", logo: statics.MAIN_URL+"/images/logos/flip_money.png", color: "00314b"},
    {id: 27, name: "Perfect money",name_pharsi: "پرفکت مانی", logo: statics.MAIN_URL+"/images/logos/perfect_money.svg", color: "dc3219"},
    {id: 1, name: "Nintendo",name_pharsi: "گیفت کارت مستر کارت", logo: statics.MAIN_URL+"/images/logos/nitentdo.png", color: "CE1D2B"},
    {id: 2, name: "Google Play Market",name_pharsi: "گیفت کارت مستر کارت", logo: statics.MAIN_URL+"/images/logos/playMarket.svg", color: "4DDECE"},
    {id: 3, name: "Amazon",name_pharsi: "گیفت کارت مستر کارت", logo: "/images/logos/amazon.png", color: "FF8F00"},
    {id: 4, name: "Steam",name_pharsi: "گیفت کارت مستر کارت", logo: "/images/logos/steam.svg", color: "383838"},
    {id: 5, name: "Microsoft",name_pharsi: "گیفت کارت مستر کارت", logo: statics.MAIN_URL+"/images/logos/microsof.svg", color: "00ACF2"},
    {id: 6, name: "Xbox Xtreme Card",name_pharsi: "گیفت کارت مستر کارت", logo: statics.MAIN_URL+"/images/logos/xbox.svg", color: "027D00"},
    {id: 7, name: "PlayStation",name_pharsi: "گیفت کارت مستر کارت", logo: statics.MAIN_URL+"/images/logos/playstation.svg", color: "494895"},
    {id: 8, name: "Apple",name_pharsi: "گیفت کارت مستر کارت", logo: statics.MAIN_URL+"/images/logos/apple.svg", color: "EC3272"},
    {id: 9, name: "Spotify",name_pharsi: "گیفت کارت مستر کارت", logo: statics.MAIN_URL+"/images/logos/spotify.svg", color: "94CF46"},
    {id: 10, name: "American Express",name_pharsi: "گیفت کارت مستر کارت", logo: statics.MAIN_URL+"/images/logos/american_express.svg", color: "016FD0"},
    {id: 11, name: "Master Card",name_pharsi: "گیفت کارت مستر کارت", logo: statics.MAIN_URL+"/images/logos/master_card.svg", color: "424242"},
    {id: 12, name: "Visa Card V",name_pharsi: "گیفت کارت مستر کارت", logo: statics.MAIN_URL+"/images/logos/visa.svg", color: "00379E"},
    {id: 13, name: "گیفت کارت مستر کارت",name_pharsi: "گیفت کارت مستر کارت", logo: statics.MAIN_URL+"/images/logos/mater_card_p.png", color: "EB4B35"},
    {id: 14, name: "Skype",name_pharsi: "گیفت کارت مستر کارت", logo: statics.MAIN_URL+"/images/logos/skype.svg", color: "00BAFA"},
    {id: 15, name: "Visa Card",name_pharsi: "گیفت کارت مستر کارت", logo: statics.MAIN_URL+"/images/logos/visa.svg", color: "00379E"},
    {id: 16, name: "GameStop",name_pharsi: "گیفت کارت مستر کارت", logo: statics.MAIN_URL+"/images/logos/game_stop.png", color: "2E2D2D"},
    {id: 17, name: "Netflix",name_pharsi: "گیفت کارت مستر کارت", logo: statics.MAIN_URL+"/images/logos/netflix.svg", color: "DD1C26"},
    {id: 18, name: "Mint",name_pharsi: "گیفت کارت مستر کارت", logo: statics.MAIN_URL+"/images/logos/mint.png", color: "639A30"},
    {id: 19, name: "Facebook",name_pharsi: "گیفت کارت مستر کارت", logo: statics.MAIN_URL+"/images/logos/facebook.svg", color: "3C5A99"},
    {id: 20, name: "Rixty",name_pharsi: "گیفت کارت مستر کارت", logo: statics.MAIN_URL+"/images/logos/rixty.png", color: "FCA71F"},
    {id: 21, name: "Orange",name_pharsi: "گیفت کارت مستر کارت", logo: statics.MAIN_URL+"/images/logos/orange.svg", color: "F15809"},
    {id: 22, name: "League Of Legends",name_pharsi: "گیفت کارت مستر کارت", logo: statics.MAIN_URL+"/images/logos/league_of_legends.png", color: "EDD017"},
    {id: 23, name: "Blizzard BattleNet",name_pharsi: "گیفت کارت مستر کارت", logo: statics.MAIN_URL+"/images/logos/blizzard_battleNet.svg", color: "008DD7"},
    {id: 24, name: "OpenBucks",name_pharsi: "گیفت کارت مستر کارت", logo: statics.MAIN_URL+"/images/logos/OpenBucks.png", color: "FEBF00"},
    {id: 25, name: "BlackBerry ID",name_pharsi: "گیفت کارت مستر کارت", logo: statics.MAIN_URL+"/images/logos/BlackBerry.png", color: "161616"},

];

module.exports.categories = categories;
